#!/bin/sh

#To run: curl https://gitlab.com/AI221/neovim-config/raw/master/setup.sh | bash

#Install stuff
echo Install curl, neovim, clang-tools
sudo apt-get install curl neovim clang-tools exuberant-ctags


#setup vim-plug
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

#download vimrc

cd

mv .config/nvim .config/old_nvim
git clone https://gitlab.com/AI221/neovim-config .config/nvim



#install plugins
nvim +PlugInstall +qa 